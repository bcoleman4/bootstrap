class UsersController < InheritedResources::Base

  private

  	before_action :authorize

    def user_params
      params.require(:user).permit(:firstname, :surname, :email, :password)
    end
end

